from datetime import datetime
class Board:
    def __init__(self):
        self.table_page = None
        self.process = None
        self.remove = True

    #adiciona uma pagina ao quadro
    def new_page(self, process, table_page):
        self.table_page = table_page
        self.process = process
        self.remove = False
        table_page.page_create(self)
        self.last_time = datetime.now()
    def update(self):
        self.remove = False
        self.last_time = datetime.now()
    
    def write(self):
        self.remove = False
        self.last_time = datetime.now()
        self.table_page.modified = True

    def delete(self):
        self.process = None
        self.remove = True
        self.table_page.page_remove()