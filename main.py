import config as configure
import primary_memory_controller.input_controller as input_controller
from primary_memory_controller.process_status_controller.process_status_controller import ProcessStatusController

config = configure.Config(128,4,0)

process_controller = ProcessStatusController(config)
for i in range(12):
    input_controller.next_instruction(process_controller, config)
    print(process_controller)
    print('\n\n\n')
    