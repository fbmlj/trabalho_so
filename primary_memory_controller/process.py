import math
import random

class Process:
    def __init__(self, name, size, page_size, config):
        self.name = name
        self.size = size
        self.table = None
        self.quantity_page =  math.ceil(size/config.page_size)
        self.cell_memory_size = config.cell_memory_size
        self.create_path()
    
    # cria o arquivo
    def create_path(self):
        file = open("memory/{}.process".format(self.name), "w")
        for i in range(self.size//self.cell_memory_size):
            file.write("{}\n".format(random.randint(0, 2**self.cell_memory_size)))
        file.close()
        
    # adiciona a tabela ao processo
    def set_table_page(self, table):
        self.table = table
        