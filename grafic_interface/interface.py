from tkinter import *
class App:
    def __init__(self, master):
        frame = Frame(master)
        frame.pack(fill=BOTH)

        def on_button():
            print("Called")
            frame.forget()
            self.newFrame()
        self.quitButton = Button(frame, text="Close", command = frame.quit)
        self.quitButton.pack(side=LEFT)

    def newFrame(self):
        print("Debug")
        frame2 = Frame()
        frame2.pack(fill=BOTH)
        self.b2 = Button(frame2, text="Yo", command=print())
        self.b2.pack()


root = Tk()
boats = App(root)
root.mainloop()