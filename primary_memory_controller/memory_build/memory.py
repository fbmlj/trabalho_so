from primary_memory_controller.memory_build.board import Board
class Memory:
    def __init__(self, page_size, page_quantity):
        self.memory_size = page_quantity * page_size
        self.page_quantity = page_quantity
        self.board_array  = []
        self.current_page = 0
        for i in range(page_quantity):
            self.board_array.append(Board())

    def get_id_of_board(self):
        for i in range(len(self.board_array)):
            if self.board_array[i].process == None:
                return i
        return None

    def remove_LRU(self):
        id_board = 0
        time_use_board = self.board_array[0].last_time
        for i in range(len(self.board_array)):
            if time_use_board > self.board_array[i].last_time:
                id_board = i
                time_use_board = self.board_array[i].last_time
        self.board_array[id_board].delete()

    def remove_CLOCK(self):
        while( not self.board_array[self.current_page].remove):
            self.board_array[self.current_page].remove = True
            self.current_page = (self.current_page + 1) % self.page_quantity
            
        self.board_array[self.current_page].delete()
        
        self.current_page = (self.current_page + 1) % self.page_quantity

