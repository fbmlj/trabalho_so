from primary_memory_controller.process_status_controller.page_status import PageStatus
from primary_memory_controller.page_controller.table_page import TablePage
from primary_memory_controller.memory_build.memory import Memory

class ProcessStatusController:
    def __init__(self, config):
        self.process_array = []
        self.current_process = 0
        self.process_name_array = []
        self.process_table_array = []
        self.memory = Memory(config.page_size, config.page_quantity)
        self.config = config
        self.current_id = 0

    # adiciona um novo processo
    def new_process(self, process, config):
        self.process_array.append(PageStatus())
        table = TablePage(process, config)
        self.process_array[-1].create(process)
        self.process_name_array.append(process.name)
        self.process_table_array.append(table)
        process.set_table_page(table)

    def suspend_process(self):
        self.process_array[self.current_process].suspend()
        self.current_process = (self.current_process + 1) % len(self.process_array)

    def in_and_out(self, process_name):
        id = self.process_id_by_name(process_name)
        self.process_array[self.current_process].change_state("bloqueado")
    # remove o processo
    ##################precisa excluir as paginas de um processo################
    def remove_process(self, process_name):
        self.process_array.pop(self.process_id_by_name(process_name))
    
    #a partir do nome exibe o id do processo dentro do array
    def process_id_by_name(self, process_name):
        return self.process_name_array.index(process_name)
    
    #condigo que corrige a falta de pagina leitura
    def change_page(self, process, address):
        
        type_of_change = self.config.type_of_remove_page
        
        id_board = self.memory.get_id_of_board()
        if id_board != None:
            aux = process.table.bring_page(address, self.current_id)
            self.memory.board_array[int(id_board)].new_page(process, aux)
            self.read_address(process.name, address)
            return
        if type_of_change == 0:
            print('lru')
            self.memory.remove_LRU()
            self.change_page(process, address)
            return
        if type_of_change == 1:
            print('relogio')
            self.memory.remove_CLOCK()
            self.change_page(process, address)
            return
        else:
            exit(1)
    #condigo que corrige a falta de pagina leitura
    def change_page_write(self, process, address):
        type_of_change = self.config.type_of_remove_page
        id_board = self.memory.get_id_of_board()
        if id_board != None:
            aux = process.table.bring_page(address, self.current_id)
            
            self.memory.board_array[int(id_board)].new_page(process, aux)
            self.write_address(process.name, address)
            return
        if type_of_change == 0:
            print('lru')
            self.memory.remove_LRU()
            self.change_page(process, address)
            return
        if type_of_change == 1:
            print('relogio')
            self.memory.remove_CLOCK()
            self.change_page(process, address)
            return
    

    #leitura do processo
    def read_address(self, process_name, address):
        id = self.process_id_by_name(process_name)
        presence, page, address_of_page, board = self.process_table_array[id].page_in_memory(address)
        if not presence:
            print("pagina nao esta na memoria")
            self.change_page(self.process_array[id].process, address)
            return
        board.update()
    #escrever no processo
    def write_address(self, process_name, address):
        id = self.process_id_by_name(process_name)
        presence, page, address_of_page, board = self.process_table_array[id].page_in_memory(address)
        if not presence:
            print("pagina nao esta na memoria")
            self.change_page_write(self.process_array[id].process, address)
            return
        board.write()
        




    def __str__(self):
        a = ""
        for i in self.process_array:
            a = a + str(i) + "\n"
        return a
