from  primary_memory_controller.page_controller.page_from_table import PageFromTable

class TablePage:
    def __init__(self, process, config):
        self.process = process
        self.table_of_page = []
        self.page_size = config.page_size
        for i in range(process.quantity_page):
            self.table_of_page.append(PageFromTable())
    
    def bring_page(self, address, board_id):
        aux = self.table_of_page[address // self.process.quantity_page]
        if not aux.presence:
            aux.page_create(board_id)
        return aux
    
    def remove_process(self, address):
        aux = self.table_of_page[address/ self.process.quantity_page]
        aux.page_remove()
    
    def page_in_memory(self, address):
        current_page = address // self.process.quantity_page
        address_of_page = address % self.page_size
        if self.table_of_page[current_page].presence:
            return True, current_page, address_of_page, self.table_of_page[current_page].location
        else:
            return False, None, None, None
    
    def suspend(self):
        for i in self.table_of_page:
            if i.remove.location != -1:
                i.remove.location.delete()