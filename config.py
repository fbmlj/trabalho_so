##################################
#       SET THE MAIN OPTION      #
##################################
class Config:
  def __init__(self, page_quantity, page_size, type_of_remove_page):
    self.page_quantity = page_quantity
    self.page_size = page_size
    self.cell_memory_size = 4
    self.type_of_remove_page = type_of_remove_page
  
