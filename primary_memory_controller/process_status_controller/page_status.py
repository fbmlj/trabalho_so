class PageStatus:
    def __init__(self):
        self.status_code = -1
        self.process = None
        self.status_array = ["pronto","pronto suspenso","bloqueado", "bloqueado suspenso","novo","inexistente"]
    #cria um estado do processo
    def create(self, process):
        self.process = process
        self.status_code = 0
    #muda o estado
    def change_state(self, new_status):
        self.status_code = self.status_array.index(new_status)
        
    #exibe o estado
    def state(self):
        return self.status_array[self.status_code]

    def suspend(self):
        self.change_state("pronto suspenso")
        self.process.table

    def __str__(self):
        return "processo: {}  estado: {}".format(self.process.name, self.state())