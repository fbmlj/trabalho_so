from primary_memory_controller.process import Process
current_line = 0
suspend = 0
#le a proxima instrução
def next_instruction(process_controller, config):
    file = open_file()
    line = file.readline()
    close_file(file)
    execute_command(line, process_controller, config)

#abre o arquivo
def open_file():
    file = open("memory/input.txt", "r")
    set_current_line(file)
    return file

#termina o arquivo
def close_file(file):
    global current_line
    current_line += 1
    file.close()

#coloca o arquivo na linha correta
def set_current_line(file):
    global current_line
    for i in range(current_line):
        file.readline()

#executa o comando lido
def execute_command(line, process_controller, config):
    global suspend
    suspend += 1
    if suspend > 5:
        suspend = 0
        print("suspend")
        process_controller.suspend_process()
    line, a, b = line.partition('\n') # retira \n da linha
    command = line.split()
    if len(command) < 1:
        return
    if command[1] == "P":#instrução da cpu
        process_controller.read_address(command[0], int(command[2]))
    elif command[1] == "I":#entrada e saida
        process_controller.in_and_out(command[0])
    elif command[1] == "C":#criar processo
        process_controller.new_process(Process(command[0],int(command[2]),1, config), config)
    elif command[1] == "R":#ler endereço logico
        process_controller.read_address(command[0], int(command[2]))
    elif command[1] == "W":#escrever endereço logico
        process_controller.write_address(command[0], int(command[2]))
    elif command[1] == "T":#terminar processo
        process_controller.remove_process(command[0])
    else:
        print("comando nao indentificado")
         
    