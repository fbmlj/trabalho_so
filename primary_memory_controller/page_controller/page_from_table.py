class PageFromTable:
    def __init__(self):
        self.presence = False
        self.modified = False
        self.location = -1

    # a pagina foi modificada
    def page_modified(self):
        self.modified = True

    # cria a pagina(valores iniciais)
    def page_create(self, page_location):
        self.presence = True
        self.location = page_location
    # remove a pagina
    def page_remove(self):
        self.presence = False
        self.location = -1

